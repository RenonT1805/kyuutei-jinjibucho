package infrastructures

import (
	"database/sql"

	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
)

const reaction_type_columns = "id, chara, point"

type ReactionTypeSqlite3 struct {
	db *sql.DB
}

func NewReactionTypeSqlite3(db *sql.DB) *ReactionTypeSqlite3 {
	return &ReactionTypeSqlite3{
		db: db,
	}
}

func (this *ReactionTypeSqlite3) FindById(id int64) (*models.ReactionType, error) {
	query := "SELECT " + reaction_type_columns + " FROM reaction_types WHERE id = ?"
	row := this.db.QueryRow(query, id)

	reaction_type := &models.ReactionType{}
	err := row.Scan(
		&reaction_type.Id,
		&reaction_type.Chara,
		&reaction_type.Point,
	)
	if err != nil {
		return nil, err
	}

	return reaction_type, nil
}

func (this *ReactionTypeSqlite3) FindAll() ([]*models.ReactionType, error) {
	query := "SELECT " + reaction_type_columns + " FROM reaction_types"
	rows, err := this.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	reaction_types := []*models.ReactionType{}
	for rows.Next() {
		reaction_type := &models.ReactionType{}
		err := rows.Scan(
			&reaction_type.Id,
			&reaction_type.Chara,
			&reaction_type.Point,
		)
		if err != nil {
			return nil, err
		}

		reaction_types = append(reaction_types, reaction_type)
	}

	return reaction_types, nil
}
