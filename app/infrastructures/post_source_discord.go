package infrastructures

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/config"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type PostSourceDiscord struct {
	session                *discordgo.Session
	db                     *sql.DB
	postRepository         *PostSqlite3
	reactionRepository     *ReactionSqlite3
	reactionTypeRepository *ReactionTypeSqlite3
	logger                 logger.Logger
}

func NewPostSourceDiscord(session *discordgo.Session, db *sql.DB) *PostSourceDiscord {
	return &PostSourceDiscord{
		session:                session,
		db:                     db,
		postRepository:         NewPostSqlite3(db),
		reactionRepository:     NewReactionSqlite3(db),
		reactionTypeRepository: NewReactionTypeSqlite3(db),
		logger:                 logger.GetLogger("post_source_discord", map[string]string{}),
	}
}

func (this *PostSourceDiscord) ImportPost(channelId string, afterDatetime time.Time) error {
	messages, err := this.session.ChannelMessages(channelId, 100, "", "", "")
	if err != nil {
		return errors.WithStack(err)
	}
	posts := []*models.Post{}
	reactions := []*models.Reaction{}
	for _, message := range messages {
		if message.Timestamp.Before(afterDatetime) {
			continue
		}
		if !strings.HasPrefix(message.Content, config.GetPostKeyword()) {
			continue
		}
		content := message.Content[len(config.GetPostKeyword()):]
		post := &models.Post{
			Id:          message.ID,
			ChannelId:   message.ChannelID,
			UserId:      message.Author.ID,
			Content:     content,
			FileURLsCSV: "",
		}
		posts = append(posts, post)
		this.logger.Debug(map[string]string{
			"message": "import post",
			"post":    fmt.Sprintf("%+v", post),
		})

		reactionTypes, err := this.reactionTypeRepository.FindAll()
		if !repositories.IsNoRowsError(err) && err != nil {
			return errors.WithStack(err)
		} else if repositories.IsNoRowsError(err) {
			this.logger.Info(map[string]string{
				"message": "no reaction type",
			})
			return nil
		}
		for _, reactionType := range reactionTypes {
			reactionUsers, err := this.session.MessageReactions(message.ChannelID, message.ID, reactionType.Chara, 100, "", "")
			if err != nil {
				return errors.WithStack(err)
			}
			for _, user := range reactionUsers {
				if user.Bot || user.ID == message.Author.ID {
					continue
				}
				reaction := &models.Reaction{
					PostId:         message.ID,
					UserId:         user.ID,
					ReactionTypeId: reactionType.Id,
					CreatedAt:      time.Now().Unix(),
				}
				reactions = append(reactions, reaction)
				this.logger.Debug(map[string]string{
					"message":  "import reaction",
					"reaction": fmt.Sprintf("%+v", reaction),
				})
			}
		}
	}

	if err := this.postRepository.StoreRows(posts); err != nil {
		return errors.WithStack(err)
	}
	err = this.reactionRepository.StoreRows(reactions)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
