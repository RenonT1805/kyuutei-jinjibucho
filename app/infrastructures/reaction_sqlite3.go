package infrastructures

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
)

const reaction_columns = "post_id, user_id, reaction_type_id, created_at"

type ReactionSqlite3 struct {
	db *sql.DB
}

func NewReactionSqlite3(db *sql.DB) *ReactionSqlite3 {
	return &ReactionSqlite3{
		db: db,
	}
}

func (this *ReactionSqlite3) FindByPostId(post_id string) ([]*models.Reaction, error) {
	query := "SELECT " + reaction_columns + " FROM reactions WHERE post_id = ?"
	rows, err := this.db.Query(query, post_id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	reactions := []*models.Reaction{}
	for rows.Next() {
		reaction := &models.Reaction{}
		err := rows.Scan(
			&reaction.PostId,
			&reaction.UserId,
			&reaction.ReactionTypeId,
			&reaction.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		reactions = append(reactions, reaction)
	}

	return reactions, nil
}

func (this *ReactionSqlite3) StoreRows(reactions []*models.Reaction) error {
	if len(reactions) == 0 {
		return nil
	}
	query := "INSERT INTO reactions (" + reaction_columns + ") VALUES "
	values := []interface{}{}
	for i, r := range reactions {
		if i > 0 {
			query += ","
		}
		query += "(?, ?, ?, ?)"
		values = append(values,
			r.PostId,
			r.UserId,
			r.ReactionTypeId,
			time.Now().Unix(),
		)
	}
	_, err := this.db.Exec(query, values...)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (this *ReactionSqlite3) Delete(post_id, user_id string) error {
	query := "DELETE FROM reactions WHERE post_id = ? AND user_id = ?"
	_, err := this.db.Exec(query, post_id, user_id)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
