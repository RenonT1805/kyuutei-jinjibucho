package infrastructures

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
)

const post_columns = "id, channel_id, user_id, content, file_urls_csv, created_at, updated_at"

type PostSqlite3 struct {
	db *sql.DB
}

func NewPostSqlite3(db *sql.DB) *PostSqlite3 {
	return &PostSqlite3{
		db: db,
	}
}

func (this *PostSqlite3) FindById(id string) (*models.Post, error) {
	query := "SELECT " + post_columns + " FROM posts WHERE id = ?"
	row := this.db.QueryRow(query, id)

	post := &models.Post{}
	err := row.Scan(
		&post.Id,
		&post.ChannelId,
		&post.UserId,
		&post.Content,
		&post.FileURLsCSV,
		&post.CreatedAt,
		&post.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (this *PostSqlite3) FindByChannelIdSinceSomePoint(channelId string, somePoint int64) ([]*models.Post, error) {
	query := "SELECT " + post_columns + " FROM posts WHERE channel_id = ? AND created_at > ? ORDER BY created_at DESC"
	rows, err := this.db.Query(query, channelId, somePoint)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := []*models.Post{}
	for rows.Next() {
		post := &models.Post{}
		err := rows.Scan(
			&post.Id,
			&post.ChannelId,
			&post.UserId,
			&post.Content,
			&post.FileURLsCSV,
			&post.CreatedAt,
			&post.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}
		posts = append(posts, post)
	}

	return posts, nil
}

func (this *PostSqlite3) GetLastPostCreatedAt(channelId string) (int64, error) {
	query := "SELECT created_at FROM posts WHERE channel_id = ? ORDER BY created_at DESC LIMIT 1"
	row := this.db.QueryRow(query, channelId)

	var createdAt int64
	err := row.Scan(&createdAt)
	if err != nil {
		return 0, err
	}

	return createdAt, nil
}

func (this *PostSqlite3) StoreRows(posts []*models.Post) error {
	if len(posts) == 0 {
		return nil
	}
	query := "INSERT INTO posts (" + post_columns + ") VALUES "
	values := []interface{}{}
	for i, s := range posts {
		if i > 0 {
			query += ","
		}
		query += "(?, ?, ?, ?, ?, ?, ?)"
		values = append(values,
			s.Id,
			s.ChannelId,
			s.UserId,
			s.Content,
			s.FileURLsCSV,
			time.Now().Unix(),
			time.Now().Unix(),
		)
	}
	_, err := this.db.Exec(query, values...)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
