package infrastructures

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
)

const stat_columns = "guild_id, user_id, point, description, created_at"

type StatSqlite3 struct {
	db *sql.DB
}

func NewStatSqlite3(db *sql.DB) *StatSqlite3 {
	return &StatSqlite3{
		db: db,
	}
}

func (this *StatSqlite3) Store(stat *models.Stat) error {
	query := "INSERT INTO stats (" + stat_columns + ") VALUES (?, ?, ?, ?, ?)"
	_, err := this.db.Exec(
		query,
		stat.GuildId,
		stat.UserId,
		stat.Point,
		stat.Description,
		time.Now().Unix(),
	)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (this *StatSqlite3) GetLastStatCreatedAt(guildId string) (int64, error) {
	query := "SELECT created_at FROM stats WHERE guild_id = ? ORDER BY created_at DESC LIMIT 1"
	row := this.db.QueryRow(query, guildId)

	var createdAt int64
	err := row.Scan(&createdAt)
	if err != nil {
		return 0, err
	}

	return createdAt, nil
}

func (this *StatSqlite3) GetSumPointsSinceSomePoint(guildId string, somePoint int64) ([]*repositories.SumPoint, error) {
	query := "SELECT user_id, SUM(point) FROM stats WHERE guild_id = ? AND created_at > ? GROUP BY user_id"
	rows, err := this.db.Query(query, guildId, somePoint)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	sumPoints := []*repositories.SumPoint{}
	for rows.Next() {
		sumPoint := &repositories.SumPoint{}
		err := rows.Scan(
			&sumPoint.UserId,
			&sumPoint.Point,
		)
		if err != nil {
			return nil, err
		}

		sumPoints = append(sumPoints, sumPoint)
	}

	return sumPoints, nil
}
