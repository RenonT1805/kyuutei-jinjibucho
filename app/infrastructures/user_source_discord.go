package infrastructures

import (
	"database/sql"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type UserSourceDiscord struct {
	session        *discordgo.Session
	db             *sql.DB
	userRepository repositories.UserRepository
	logger         logger.Logger
}

func NewUserSourceDiscord(session *discordgo.Session, db *sql.DB) *UserSourceDiscord {
	return &UserSourceDiscord{
		session:        session,
		db:             db,
		userRepository: NewUserSqlite3(db),
		logger:         logger.GetLogger("user_source_discord", map[string]string{}),
	}
}

func (this *UserSourceDiscord) ImportUser(guildId string) ([]*models.User, error) {
	members, err := this.session.GuildMembers(guildId, "", 1000)
	if err != nil {
		return nil, err
	}
	users := make([]*models.User, 0, len(members))
	for _, member := range members {
		if member.User.Bot {
			continue
		}
		users = append(users, &models.User{
			Id:       member.User.ID,
			GuildId:  guildId,
			Username: member.User.Username,
		})
		this.logger.Debug(map[string]string{
			"message": "import user",
			"guild":   guildId,
			"user":    member.User.ID,
		})
	}
	err = this.userRepository.StoreRows(users)
	if err != nil {
		return nil, err
	}
	return users, nil
}
