package infrastructures

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
)

const user_columns = "id, guild_id, username, created_at, updated_at"

type UserSqlite3 struct {
	db *sql.DB
}

func NewUserSqlite3(db *sql.DB) *UserSqlite3 {
	return &UserSqlite3{
		db: db,
	}
}

func (this *UserSqlite3) FindById(id string) (*models.User, error) {
	query := "SELECT " + user_columns + " FROM users WHERE id = ?"
	row := this.db.QueryRow(query, id)

	user := &models.User{}
	err := row.Scan(
		&user.Id,
		&user.GuildId,
		&user.Username,
		&user.CreatedAt,
		&user.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (this *UserSqlite3) FindByGuildId(guildId string) ([]*models.User, error) {
	query := "SELECT " + user_columns + " FROM users WHERE guild_id = ?"
	rows, err := this.db.Query(query, guildId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := []*models.User{}
	for rows.Next() {
		user := &models.User{}
		err := rows.Scan(
			&user.Id,
			&user.GuildId,
			&user.Username,
			&user.CreatedAt,
			&user.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}

func (this *UserSqlite3) StoreRows(user []*models.User) error {
	if len(user) == 0 {
		return nil
	}
	query := "INSERT INTO users (" + user_columns + ") VALUES "
	values := []interface{}{}
	for i, u := range user {
		if i > 0 {
			query += ","
		}
		query += "(?, ?, ?, ?, ?)"
		values = append(values, u.Id, u.GuildId, u.Username, time.Now().Unix(), time.Now().Unix())
	}
	_, err := this.db.Exec(query, values...)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (this *UserSqlite3) Delete(id string) error {
	query := "DELETE FROM users WHERE id = ?"
	_, err := this.db.Exec(query, id)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
