package infrastructures

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
)

const guild_columns = "id, channel_id, day_of_week, created_at, updated_at"

type GuildSqlite3 struct {
	db *sql.DB
}

func NewGuildSqlite3(db *sql.DB) *GuildSqlite3 {
	return &GuildSqlite3{
		db: db,
	}
}

func (this *GuildSqlite3) FindById(id string) (*models.Guild, error) {
	query := "SELECT " + guild_columns + " FROM guilds WHERE id = ?"
	row := this.db.QueryRow(query, id)

	guild := &models.Guild{}
	err := row.Scan(
		&guild.Id,
		&guild.ChannelId,
		&guild.DayOfWeek,
		&guild.CreatedAt,
		&guild.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return guild, nil
}

func (this *GuildSqlite3) FindAll() ([]*models.Guild, error) {
	query := "SELECT " + guild_columns + " FROM guilds"
	rows, err := this.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	guilds := []*models.Guild{}
	for rows.Next() {
		guild := &models.Guild{}
		err := rows.Scan(
			&guild.Id,
			&guild.ChannelId,
			&guild.DayOfWeek,
			&guild.CreatedAt,
			&guild.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}
		guilds = append(guilds, guild)
	}

	return guilds, nil
}

func (this *GuildSqlite3) Store(guild *models.Guild) error {
	query := "INSERT INTO guilds (" + guild_columns + ") VALUES (?, ?, ?, ?, ?)"
	_, err := this.db.Exec(
		query,
		guild.Id,
		guild.ChannelId,
		guild.DayOfWeek,
		time.Now().Unix(),
		time.Now().Unix(),
	)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (this *GuildSqlite3) Delete(id string) error {
	query := "DELETE FROM guilds WHERE id = ?"
	_, err := this.db.Exec(query, id)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
