package db

import (
	"database/sql"

	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"

	"gitlab.com/RenonT1805/kyuutei-jinjibucho/config"

	_ "github.com/mattn/go-sqlite3"
)

func NewSQLite3DB() (*sql.DB, error) {
	logger := logger.GetLogger("sqlite3_db", map[string]string{})
	appdir := config.GetAppDir()
	dbPath := appdir + config.SHORT_APP_NAME + ".db"
	logger.Debug(map[string]string{
		"db path": dbPath,
	})
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		return nil, err
	}
	return db, nil
}
