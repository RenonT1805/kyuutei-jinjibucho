CREATE TABLE IF NOT EXISTS guilds (
    id TEXT UNIQUE NOT NULL,
    channel_id TEXT PRIMARY KEY NOT NULL,
    day_of_week INTEGER NOT NULL,
    created_at INTEGER NOT NULL,
    updated_at INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
    id TEXT NOT NULL,
    guild_id TEXT NOT NULL,
    username TEXT NOT NULL,
    created_at INTEGER NOT NULL,
    updated_at INTEGER NOT NULL,
    PRIMARY KEY (id, guild_id)
);

CREATE TABLE IF NOT EXISTS stats (
    guild_id TEXT NOT NULL,
    user_id TEXT NOT NULL,
    point INTEGER NOT NULL,
    description TEXT NOT NULL,
    created_at INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS posts (
    id TEXT PRIMARY KEY NOT NULL,
    channel_id TEXT NOT NULL,
    user_id TEXT NOT NULL,
    content TEXT,
    file_urls_csv TEXT,
    created_at INTEGER NOT NULL,
    updated_at INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS reactions (
    post_id TEXT NOT NULL,
    user_id TEXT NOT NULL,
    reaction_type_id INTEGER NOT NULL,
    created_at INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS reaction_types (
    id INTEGER PRIMARY KEY NOT NULL,
    chara TEXT NOT NULL,
    point INTEGER NOT NULL
);

INSERT INTO
    reaction_types (id, chara, point)
VALUES
    (1, "👍", 2),
    (2, "👎", 0);