package controllers

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type EvaluationBatchController struct {
	postApplication       *application.PostApplication
	evaluationApplication *application.EvaluationApplication
	ctx                   *contexts.BatchContext
	logger                logger.Logger
}

func NewEvaluationBatchController(
	postApplication *application.PostApplication,
	evaluationApplication *application.EvaluationApplication,
	ctx *contexts.BatchContext,
) *EvaluationBatchController {
	return &EvaluationBatchController{
		postApplication:       postApplication,
		evaluationApplication: evaluationApplication,
		logger:                logger.GetLogger("evaluation_batch_controller", map[string]string{}),
		ctx:                   ctx,
	}
}

func (this *EvaluationBatchController) Evaluation() error {
	logger := this.logger.GetLogger("evaluation", map[string]string{})
	logger.Info(map[string]string{
		"message": "start evaluation",
	})
	defer logger.Info(map[string]string{
		"message": "end evaluation",
	})
	err := this.postApplication.ImportPost()
	if err != nil {
		return errors.WithStack(err)
	}
	err = this.evaluationApplication.Evaluation(time.Now().UTC(), true)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
