package controllers

import (
	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/controllers/params"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/infrastructures"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/presenters"
)

type GuildDiscordCommandController struct {
	guildApplication *application.GuildApplication
	ctx              *contexts.DiscordCommandContext
	logger           logger.Logger
}

func NewGuildDiscordCommandController(
	ctx *contexts.DiscordCommandContext,
) *GuildDiscordCommandController {
	return &GuildDiscordCommandController{
		guildApplication: application.NewGuildApplication(
			presenters.NewGuildDiscordCommandPresenter(ctx),
			infrastructures.NewGuildSqlite3(ctx.GetDB()),
			infrastructures.NewUserSourceDiscord(ctx.GetDiscordSession(), ctx.GetDB()),
		),
		logger: logger.GetLogger("guild_discord_command_controller", map[string]string{
			"interaction_id": ctx.GetInteraction().ID,
		}),
		ctx: ctx,
	}
}

func (this *GuildDiscordCommandController) Register(params *params.DiscordCommandGuildRegisterParam) error {
	logger := this.logger.GetLogger("register", map[string]string{})
	logger.Info(map[string]string{
		"message": "start register guild",
	})
	defer logger.Info(map[string]string{
		"message": "end register guild",
	})
	err := this.guildApplication.Register(
		this.ctx.GetInteraction().GuildID,
		this.ctx.GetInteraction().ChannelID,
		int64(params.DayOfWeek),
	)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (this *GuildDiscordCommandController) Users(params *params.DiscordCommandGuildRegisterParam) error {
	logger := this.logger.GetLogger("users", map[string]string{})
	logger.Info(map[string]string{
		"message": "users register guild",
	})
	defer logger.Info(map[string]string{
		"message": "users register guild",
	})
	err := this.guildApplication.Register(
		this.ctx.GetInteraction().GuildID,
		this.ctx.GetInteraction().ChannelID,
		int64(params.DayOfWeek),
	)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
