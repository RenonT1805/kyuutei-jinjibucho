package params

import (
	"reflect"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/controllers/validators"
)

func ParseDiscordCommandParam[T interface{}](ctx *contexts.DiscordCommandContext) (T, error) {
	options := ctx.GetInteraction().ApplicationCommandData().Options
	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	var value T
	numField := reflect.TypeOf(value).NumField()
	for i := 0; i < numField; i++ {
		field := reflect.TypeOf(value).Field(i)
		paramName := field.Tag.Get("json")
		if paramName == "" {
			paramName = field.Name
		}

		options := ctx.GetInteraction().ApplicationCommandData().Options
		optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
		for _, opt := range options {
			optionMap[opt.Name] = opt
		}

		if option, ok := optionMap[paramName]; ok {
			switch field.Type.Kind() {
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				reflect.ValueOf(&value).Elem().Field(i).SetInt(option.IntValue())
			case reflect.String:
				reflect.ValueOf(&value).Elem().Field(i).SetString(option.StringValue())
			case reflect.Bool:
				reflect.ValueOf(&value).Elem().Field(i).SetBool(option.BoolValue())
			case reflect.Float32, reflect.Float64:
				reflect.ValueOf(&value).Elem().Field(i).SetFloat(option.FloatValue())
			case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				reflect.ValueOf(&value).Elem().Field(i).SetUint(option.UintValue())
			}
		}
	}
	return value, validators.DefaultValidation(value)
}
