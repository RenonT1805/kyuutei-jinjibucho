package params

type DiscordCommandStatParamAggregationPeriod int64

const (
	DiscordCommandStatParamMonth DiscordCommandStatParamAggregationPeriod = iota
	DiscordCommandStatParamTotal
)

type DiscordCommandStatParam struct {
	AggregationPeriod DiscordCommandStatParamAggregationPeriod `json:"aggregation_period" required:"true"`
}
