package controllers

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type EvaluationCronController struct {
	postApplication       *application.PostApplication
	evaluationApplication *application.EvaluationApplication
	ctx                   *contexts.CronContext
	logger                logger.Logger
}

func NewEvaluationCronController(
	postApplication *application.PostApplication,
	evaluationApplication *application.EvaluationApplication,
	ctx *contexts.CronContext,
) *EvaluationCronController {
	return &EvaluationCronController{
		postApplication:       postApplication,
		evaluationApplication: evaluationApplication,
		logger:                logger.GetLogger("evaluation_cron_controller", map[string]string{}),
		ctx:                   ctx,
	}
}

func (this *EvaluationCronController) Evaluation() error {
	logger := this.logger.GetLogger("evaluation", map[string]string{})
	logger.Info(map[string]string{
		"message": "start evaluation",
	})
	defer logger.Info(map[string]string{
		"message": "end evaluation",
	})
	err := this.postApplication.ImportPost()
	if err != nil {
		return errors.WithStack(err)
	}
	err = this.evaluationApplication.Evaluation(time.Now().UTC(), false)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
