package controllers

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/controllers/params"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/infrastructures"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/presenters"
)

type StatDiscordCommandController struct {
	statApplication *application.StatApplication
	ctx             *contexts.DiscordCommandContext
	logger          logger.Logger
}

func NewStatDiscordCommandController(
	ctx *contexts.DiscordCommandContext,
) *StatDiscordCommandController {
	return &StatDiscordCommandController{
		statApplication: application.NewStatApplication(
			presenters.NewStatDiscordCommandPresenter(ctx),
			infrastructures.NewUserSqlite3(ctx.GetDB()),
			infrastructures.NewStatSqlite3(ctx.GetDB()),
		),
		logger: logger.GetLogger("stat_discord_command_controller", map[string]string{}),
		ctx:    ctx,
	}
}

func (this *StatDiscordCommandController) Stat(param *params.DiscordCommandStatParam) error {
	logger := this.logger.GetLogger("stat", map[string]string{})
	logger.Info(map[string]string{
		"message": "start stat",
	})
	defer logger.Info(map[string]string{
		"message": "end stat",
	})
	since := int64(0)
	switch param.AggregationPeriod {
	case params.DiscordCommandStatParamMonth:
		dt := time.Now().UTC()
		since = time.Date(dt.Year(), dt.Month(), 0, 0, 0, 0, 0, time.UTC).Unix()
	case params.DiscordCommandStatParamTotal:
		since = 0
	}
	err := this.statApplication.Stat(this.ctx.GetInteraction().GuildID, since)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
