package application

import (
	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
)

type GuildRegisterOutput struct {
	AlreadyRegistered bool
	ImportUser        []*models.User
}

type GuildPresenter interface {
	RegisterComplete(output *GuildRegisterOutput) error
}

type GuildApplication struct {
	presenter            GuildPresenter
	guildRepository      repositories.GuildRepository
	userSourceRepository repositories.UserSourceRepository
}

func NewGuildApplication(
	presenter GuildPresenter,
	guildRepository repositories.GuildRepository,
	userSourceRepository repositories.UserSourceRepository,
) *GuildApplication {
	return &GuildApplication{
		presenter:            presenter,
		guildRepository:      guildRepository,
		userSourceRepository: userSourceRepository,
	}
}

func (this *GuildApplication) Register(guildId string, channelId string, dayOfWeek int64) error {
	err := this.guildRepository.Store(&models.Guild{
		Id:        guildId,
		ChannelId: channelId,
		DayOfWeek: dayOfWeek,
	})
	if repositories.IsUniqueConstraintError(err) {
		return this.presenter.RegisterComplete(&GuildRegisterOutput{
			AlreadyRegistered: true,
		})
	} else if err != nil {
		return errors.WithStack(err)
	}

	users, err := this.userSourceRepository.ImportUser(guildId)
	if err != nil {
		return errors.WithStack(err)
	}

	err = this.presenter.RegisterComplete(&GuildRegisterOutput{
		AlreadyRegistered: false,
		ImportUser:        users,
	})
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
