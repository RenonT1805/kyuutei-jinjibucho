package application

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type EvaluationEvaluationOutput struct {
	ChannelUserScores map[string]map[string][]*models.Stat
}

type EvaluationPresenter interface {
	EvaluationComplete(output *EvaluationEvaluationOutput) error
}

type EvaluationApplication struct {
	presenter              EvaluationPresenter
	guildRepository        repositories.GuildRepository
	userRepository         repositories.UserRepository
	postRepository         repositories.PostRepository
	reactionRepository     repositories.ReactionRepository
	reactionTypeRepository repositories.ReactionTypeRepository
	statRepository         repositories.StatRepository
	logger                 logger.Logger
}

func NewEvaluationApplication(
	presenter EvaluationPresenter,
	guildRepository repositories.GuildRepository,
	userRepository repositories.UserRepository,
	postRepository repositories.PostRepository,
	reactionRepository repositories.ReactionRepository,
	reactionTypeRepository repositories.ReactionTypeRepository,
	statRepository repositories.StatRepository,
) *EvaluationApplication {
	return &EvaluationApplication{
		presenter:              presenter,
		guildRepository:        guildRepository,
		userRepository:         userRepository,
		postRepository:         postRepository,
		reactionRepository:     reactionRepository,
		reactionTypeRepository: reactionTypeRepository,
		statRepository:         statRepository,
		logger:                 logger.GetLogger("evaluation_application", map[string]string{}),
	}
}

const (
	PostPoint     = 5
	ReactionPoint = 2
)

func (this *EvaluationApplication) Evaluation(dt time.Time, force bool) error {
	this.logger.Info(map[string]string{
		"message": "start evaluation",
		"dt":      fmt.Sprintf("%v", dt),
	})
	// TODOO: JSTがハードコートされているので、要修正
	today := time.Date(dt.Year(), dt.Month(), dt.Day(), 9, 0, 0, 0, time.UTC)
	guilds, err := this.guildRepository.FindAll()
	if !repositories.IsNoRowsError(err) && err != nil {
		return errors.WithStack(err)
	} else if repositories.IsNoRowsError(err) {
		this.logger.Info(map[string]string{
			"message": "no guild",
		})
		return nil
	}
	output := EvaluationEvaluationOutput{
		ChannelUserScores: map[string]map[string][]*models.Stat{},
	}
	for _, guild := range guilds {
		output.ChannelUserScores[guild.ChannelId] = map[string][]*models.Stat{}

		//そのチャンネルの評価曜日でない場合はスキップする
		if guild.DayOfWeek != int64(today.Weekday()) && !force {
			continue
		}

		// 前回の評価から１日以上経過していない場合はスキップする
		lastStatCreatedAt, err := this.statRepository.GetLastStatCreatedAt(guild.Id)
		if !repositories.IsNoRowsError(err) && err != nil {
			return errors.WithStack(err)
		}
		if lastStatCreatedAt > today.Unix() && !force {
			continue
		}

		this.logger.Info(map[string]string{
			"lastStatCreatedAt": fmt.Sprintf("%v", lastStatCreatedAt),
			"today":             fmt.Sprintf("%v", today.Unix()),
		})

		// そのチャンネルの投稿を取得する
		posts, err := this.postRepository.FindByChannelIdSinceSomePoint(guild.ChannelId, lastStatCreatedAt)
		if !repositories.IsNoRowsError(err) && err != nil {
			return errors.WithStack(err)
		}
		this.logger.Info(map[string]string{
			"posts": fmt.Sprintf("%v", posts),
		})

		stats := []*models.Stat{}
		for _, post := range posts {
			// 投稿者にスコアを加算
			stats = append(stats, &models.Stat{
				GuildId:     guild.Id,
				UserId:      post.UserId,
				Point:       PostPoint,
				Description: "進捗を出した",
			})

			// その投稿についたリアクションを取得する
			reactions, err := this.reactionRepository.FindByPostId(post.Id)
			if !repositories.IsNoRowsError(err) && err != nil {
				return errors.WithStack(err)
			} else if repositories.IsNoRowsError(err) {
				this.logger.Info(map[string]string{
					"message": "no reactions",
					"post":    post.Id,
				})
				continue
			}
			for _, reaction := range reactions {
				// そのリアクションの種類を取得する
				reactionType, err := this.reactionTypeRepository.FindById(reaction.ReactionTypeId)
				if !repositories.IsNoRowsError(err) && err != nil {
					return errors.WithStack(err)
				} else if repositories.IsNoRowsError(err) {
					this.logger.Info(map[string]string{
						"message": "no reaction type",
					})
					continue
				}
				// そのリアクションの種類のスコアを加算
				stats = append(stats, &models.Stat{
					GuildId:     guild.Id,
					UserId:      post.UserId,
					Point:       reactionType.Point,
					Description: "リアクション " + reactionType.Chara + " を取得",
				})
				// リアクションをつけたユーザーにもスコアを加算
				stats = append(stats, &models.Stat{
					GuildId:     guild.Id,
					UserId:      reaction.UserId,
					Point:       ReactionPoint,
					Description: "他人へのリアクション",
				})
			}
		}

		users, err := this.userRepository.FindByGuildId(guild.Id)

		// 進捗がない場合は、空のスコアを追加する
		if len(stats) == 0 {
			for _, user := range users {
				stats = append(stats, &models.Stat{
					GuildId:     guild.Id,
					UserId:      user.Id,
					Point:       0,
					Description: "進捗なし",
				})
			}
		}

		if !repositories.IsNoRowsError(err) && err != nil {
			return errors.WithStack(err)
		} else if repositories.IsNoRowsError(err) {
			this.logger.Info(map[string]string{
				"message": "no user",
				"guild":   guild.Id,
			})
			return nil
		}
		for _, user := range users {
			output.ChannelUserScores[guild.ChannelId][user.Id] = []*models.Stat{}
		}
		for _, stat := range stats {
			output.ChannelUserScores[guild.ChannelId][stat.UserId] = append(
				output.ChannelUserScores[guild.ChannelId][stat.UserId], stat,
			)
			err := this.statRepository.Store(stat)
			if err != nil {
				return errors.WithStack(err)
			}
		}
	}
	jsonData, err := json.Marshal(output)
	if err != nil {
		return errors.WithStack(err)
	}
	this.logger.Info(map[string]string{
		"output": string(jsonData),
	})
	err = this.presenter.EvaluationComplete(&output)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
