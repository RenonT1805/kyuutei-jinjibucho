package application

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type PostApplication struct {
	guildRepository      repositories.GuildRepository
	postRepository       repositories.PostRepository
	postSourceRepository repositories.PostSourceRepository
	logger               logger.Logger
}

func NewPostApplication(
	guildRepository repositories.GuildRepository,
	postRepository repositories.PostRepository,
	postSourceRepository repositories.PostSourceRepository,
) *PostApplication {
	return &PostApplication{
		guildRepository:      guildRepository,
		postRepository:       postRepository,
		postSourceRepository: postSourceRepository,
		logger:               logger.GetLogger("post_application", map[string]string{}),
	}
}

func (this *PostApplication) ImportPost() error {
	guilds, err := this.guildRepository.FindAll()
	if !repositories.IsNoRowsError(err) && err != nil {
		return errors.WithStack(err)
	} else if repositories.IsNoRowsError(err) {
		this.logger.Info(map[string]string{
			"message": "no guild",
		})
		return nil
	}
	for _, guild := range guilds {
		lastPostCreatedAt, err := this.postRepository.GetLastPostCreatedAt(guild.ChannelId)
		if !repositories.IsNoRowsError(err) && err != nil {
			return errors.WithStack(err)
		} else if repositories.IsNoRowsError(err) {
			lastPostCreatedAt = 0
			this.logger.Info(map[string]string{
				"message": "no imported post",
				"guild":   guild.Id,
			})
		}
		this.logger.Info(map[string]string{
			"message": "import post",
			"guild":   guild.Id,
			"after":   time.Unix(lastPostCreatedAt, 0).String(),
		})
		err = this.postSourceRepository.ImportPost(guild.ChannelId, time.Unix(lastPostCreatedAt, 0))
		if err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}
