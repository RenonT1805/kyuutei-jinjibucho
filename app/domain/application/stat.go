package application

import (
	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type StatStatOutput struct {
	UserSumPoints map[string]int
	Since         int64
}

type StatPresenter interface {
	StatComplete(output *StatStatOutput) error
}

type StatApplication struct {
	presenter      StatPresenter
	userRepository repositories.UserRepository
	statRepository repositories.StatRepository
	logger         logger.Logger
}

func NewStatApplication(
	presenter StatPresenter,
	userRepository repositories.UserRepository,
	statRepository repositories.StatRepository,
) *StatApplication {
	return &StatApplication{
		presenter:      presenter,
		userRepository: userRepository,
		statRepository: statRepository,
		logger:         logger.GetLogger("stat_application", map[string]string{}),
	}
}

func (this *StatApplication) Stat(guildId string, since int64) error {
	this.logger.Info(map[string]string{
		"message": "start stat",
	})
	defer this.logger.Info(map[string]string{
		"message": "end stat",
	})
	users, err := this.userRepository.FindByGuildId(guildId)
	if err != nil {
		return errors.WithStack(err)
	}
	sumPoints, err := this.statRepository.GetSumPointsSinceSomePoint(guildId, since)
	if err != nil {
		return errors.WithStack(err)
	}
	output := &StatStatOutput{
		UserSumPoints: map[string]int{},
		Since:         since,
	}
	for _, user := range users {
		for _, sumPoint := range sumPoints {
			if user.Id == sumPoint.UserId {
				output.UserSumPoints[user.Username] = sumPoint.Point
			}
		}
	}
	err = this.presenter.StatComplete(output)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
