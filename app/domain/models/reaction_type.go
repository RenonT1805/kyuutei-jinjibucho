package models

type ReactionType struct {
	Id    int64  `db:"id" json:"id"`
	Chara string `db:"chara" json:"chara"`
	Point int64  `db:"point" json:"point"`
}
