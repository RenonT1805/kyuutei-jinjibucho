package models

type Post struct {
	Id          string `db:"id" json:"id"`
	ChannelId   string `db:"channel_id" json:"channel_id"`
	UserId      string `db:"user_id" json:"user_id"`
	Content     string `db:"content" json:"content"`
	FileURLsCSV string `db:"file_urls_csv" json:"file_urls_csv"`
	CreatedAt   int64  `db:"created_at" json:"created_at"`
	UpdatedAt   int64  `db:"updated_at" json:"updated_at"`
}
