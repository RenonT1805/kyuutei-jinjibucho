package models

type Stat struct {
	GuildId     string `db:"guild_id" json:"guild_id"`
	UserId      string `db:"user_id" json:"user_id"`
	Point       int64  `db:"point" json:"point"`
	Description string `db:"description" json:"description"`
	CreatedAt   int64  `db:"created_at" json:"created_at"`
}
