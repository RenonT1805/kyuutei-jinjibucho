package models

type Guild struct {
	Id        string `db:"id" json:"id"`
	ChannelId string `db:"channel_id" json:"channel_id"`
	DayOfWeek int64  `db:"day_of_week" json:"day_of_week"`
	CreatedAt int64  `db:"created_at" json:"created_at"`
	UpdatedAt int64  `db:"updated_at" json:"updated_at"`
}
