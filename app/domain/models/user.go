package models

type User struct {
	Id        string `db:"id" json:"id"`
	GuildId   string `db:"guild_id" json:"guild_id"`
	Username  string `db:"username" json:"username"`
	CreatedAt int64  `db:"created_at" json:"created_at"`
	UpdatedAt int64  `db:"updated_at" json:"updated_at"`
}
