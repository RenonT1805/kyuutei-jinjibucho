package models

type Reaction struct {
	PostId         string `db:"post_id" json:"post_id"`
	UserId         string `db:"user_id" json:"user_id"`
	ReactionTypeId int64  `db:"reaction_type_id" json:"reaction_type_id"`
	CreatedAt      int64  `db:"created_at" json:"created_at"`
}
