package repositories

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"

type ReactionTypeRepository interface {
	FindById(id int64) (*models.ReactionType, error)
}
