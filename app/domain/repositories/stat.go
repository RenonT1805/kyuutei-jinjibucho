package repositories

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"

type SumPoint struct {
	UserId string
	Point  int
}

type StatRepository interface {
	Store(stat *models.Stat) error
	GetLastStatCreatedAt(guildId string) (int64, error)
	GetSumPointsSinceSomePoint(guildId string, somePoint int64) ([]*SumPoint, error)
}
