package repositories

import "time"

type PostSourceRepository interface {
	ImportPost(channelId string, afterDatetime time.Time) error
}
