package repositories

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"

type UserRepository interface {
	FindById(id string) (*models.User, error)
	FindByGuildId(guildId string) ([]*models.User, error)
	StoreRows(user []*models.User) error
	Delete(id string) error
}
