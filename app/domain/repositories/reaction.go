package repositories

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"

type ReactionRepository interface {
	FindByPostId(post_id string) ([]*models.Reaction, error)
	Delete(reaction_id, user_id string) error
}
