package repositories

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"

type GuildRepository interface {
	FindById(id string) (*models.Guild, error)
	FindAll() ([]*models.Guild, error)
	Store(guild *models.Guild) error
	Delete(id string) error
}
