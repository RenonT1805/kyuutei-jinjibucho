package repositories

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"

type UserSourceRepository interface {
	ImportUser(guildId string) ([]*models.User, error)
}
