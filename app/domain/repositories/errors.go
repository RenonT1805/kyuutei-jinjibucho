package repositories

import (
	"database/sql"
	"strings"

	"github.com/pkg/errors"
)

func IsNoRowsError(err error) bool {
	return errors.Is(err, sql.ErrNoRows)
}

func IsUniqueConstraintError(err error) bool {
	return err != nil && strings.HasPrefix(errors.Unwrap(err).Error(), "UNIQUE constraint failed")
}
