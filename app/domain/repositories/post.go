package repositories

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/models"

type PostRepository interface {
	FindById(id string) (*models.Post, error)
	FindByChannelIdSinceSomePoint(channelId string, somePoint int64) ([]*models.Post, error)
	GetLastPostCreatedAt(channelId string) (int64, error)
}
