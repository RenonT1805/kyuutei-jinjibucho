package main

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/config"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/controllers"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/db"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/infrastructures"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

type EvaluationBatchPresenter struct {
	ctx    *contexts.BatchContext
	logger logger.Logger
}

func (this *EvaluationBatchPresenter) EvaluationComplete(output *application.EvaluationEvaluationOutput) error {
	for guildId, userScore := range output.ChannelUserScores {
		message := "今週の進捗の確認を行う\n"
		for userId, stats := range userScore {
			user, err := this.ctx.GetDiscordSession().User(userId)
			if err != nil {
				return errors.WithStack(err)
			}

			message += "\n" + user.Username + " の評価は以下の通りだ。\n"
			sum := 0
			for _, stat := range stats {
				message += "- " + stat.Description + " (" + strconv.Itoa(int(stat.Point)) + " ポイント)\n"
				sum += int(stat.Point)
			}

			if 0 < len(stats) {
				message += "合計 " + strconv.Itoa(sum) + " ポイント\n"
			} else {
				message += "お前はこの1週間何もしていない。無能はこのグループには必要ない。\n"
			}
		}

		message += "以上だ。引き続き進捗を出せ。"

		notification, err := config.GetEvaluationBatchNotification()
		if err != nil {
			return errors.WithStack(err)
		}

		if strings.ToUpper(notification) == "TRUE" {
			_, err := this.ctx.GetDiscordSession().ChannelMessageSend(guildId, message)
			if err != nil {
				return errors.WithStack(err)
			}
		} else {
			this.logger.Info(map[string]string{
				"complete_message": message,
			})
		}
	}
	return nil
}

func main() {
	err := logger.SetupLogger()
	if err != nil {
		log.Panicln(err)
	}
	defer logger.CloseLogger()

	batchLogger := logger.GetLogger("evaluation_batch", map[string]string{})
	batchLogger.Info(map[string]string{
		"message": "start evaluation batch",
	})
	defer batchLogger.Info(map[string]string{
		"message": "end evaluation batch",
	})

	db, err := db.NewSQLite3DB()
	if err != nil {
		batchLogger.Error(map[string]string{
			"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
		})
		return
	}
	defer db.Close()

	token, err := config.GetBotToken()
	if err != nil {
		batchLogger.Error(map[string]string{
			"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
		})
		return
	}

	discord, err := discordgo.New(token)
	if err != nil {
		batchLogger.Error(map[string]string{
			"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
		})
		return
	}

	ctx := contexts.NewBatchContext(
		context.Background(),
		discord,
		db,
	)
	evaluationController := controllers.NewEvaluationBatchController(
		application.NewPostApplication(
			infrastructures.NewGuildSqlite3(ctx.GetDB()),
			infrastructures.NewPostSqlite3(ctx.GetDB()),
			infrastructures.NewPostSourceDiscord(ctx.GetDiscordSession(), ctx.GetDB()),
		),
		application.NewEvaluationApplication(
			&EvaluationBatchPresenter{
				ctx:    ctx,
				logger: batchLogger.GetLogger("evaluation_batch_presenter", map[string]string{}),
			},
			infrastructures.NewGuildSqlite3(ctx.GetDB()),
			infrastructures.NewUserSqlite3(ctx.GetDB()),
			infrastructures.NewPostSqlite3(ctx.GetDB()),
			infrastructures.NewReactionSqlite3(ctx.GetDB()),
			infrastructures.NewReactionTypeSqlite3(ctx.GetDB()),
			infrastructures.NewStatSqlite3(ctx.GetDB()),
		),
		ctx,
	)

	err = evaluationController.Evaluation()
	if err != nil {
		batchLogger.Error(map[string]string{
			"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
		})
		return
	}
}
