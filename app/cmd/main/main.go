package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"dario.cat/mergo"
	"github.com/bwmarrin/discordgo"
	"github.com/robfig/cron/v3"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/config"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/db"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/handlers"
	logger "gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

func main() {
	err := logger.SetupLogger()
	if err != nil {
		log.Panicln(err)
	}
	defer logger.CloseLogger()

	initializeLogger := logger.GetLogger("initialize", map[string]string{})

	token, err := config.GetBotToken()
	if err != nil {
		initializeLogger.Fatal(map[string]string{
			"message": err.Error(),
		})
		log.Panicln(err)
	}

	discord, err := discordgo.New(token)
	if err != nil {
		initializeLogger.Fatal(map[string]string{
			"message": err.Error(),
		})
		log.Panicln(err)
	}

	discord.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		readyHandlerLogger := logger.GetLogger("ready_handler", map[string]string{
			"session_id": r.SessionID,
		})
		readyHandlerLogger.Info(map[string]string{
			"username":      s.State.User.Username,
			"discriminator": s.State.User.Discriminator,
		})

		// コマンド登録
		for _, guild := range r.Guilds {
			commandMap := map[string]struct{}{}
			for _, command := range handlers.GetDiscordCommandCommands() {
				commandName, err := discord.ApplicationCommandCreate(s.State.User.ID, guild.ID, command)
				if err != nil {
					readyHandlerLogger.Fatal(map[string]string{
						"message": err.Error(),
					})
					log.Panicln(err)
				}
				commandMap[commandName.Name] = struct{}{}
			}

			commands, err := discord.ApplicationCommands(s.State.User.ID, guild.ID)
			if err != nil {
				if err != nil {
					readyHandlerLogger.Fatal(map[string]string{
						"message": err.Error(),
					})
					log.Panicln(err)
				}
			}

			// 登録されたコマンド以外は削除
			for _, command := range commands {
				if _, ok := commandMap[command.Name]; ok {
					continue
				}
				err := discord.ApplicationCommandDelete(s.State.User.ID, guild.ID, command.ID)
				if err != nil {
					if err != nil {
						readyHandlerLogger.Fatal(map[string]string{
							"message": err.Error(),
						})
						log.Panicln(err)
					}
				}
			}
		}
	})

	discord.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		interactionLogger := logger.GetLogger("interaction_handler", map[string]string{
			"interaction_id": i.ID,
		})
		defer interactionLogger.Info(map[string]string{
			"message": "finished",
		})

		logdata := map[string]string{}
		for _, option := range i.ApplicationCommandData().Options {
			logdata["options."+option.Name] = fmt.Sprintf("%#v", option.Value)
		}
		mergo.Merge(&logdata, map[string]string{
			"user_id":    i.Member.User.ID,
			"channel_id": i.GuildID,
			"command":    i.ApplicationCommandData().Name,
		})
		interactionLogger.Info(logdata)

		if h, ok := handlers.GetDiscordCommandCommandHandlers()[i.ApplicationCommandData().Name]; ok {
			// DB初期化
			db, err := db.NewSQLite3DB()
			if err != nil {
				discord.ChannelMessageSend(i.GuildID, "エラーが発生しました")
				interactionLogger.Error(map[string]string{
					"message": err.Error(),
				})
				return
			}
			defer db.Close()

			h(contexts.NewDiscordCommandContext(
				context.Background(),
				s,
				i,
				db,
			))
		}
	})

	discord.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		messageLogger := logger.GetLogger("message_handler", map[string]string{
			"message_id": m.ID,
		})

		if m.Author.Bot {
			return
		}

		// DB初期化
		db, err := db.NewSQLite3DB()
		if err != nil {
			discord.ChannelMessageSend(m.ChannelID, "エラーが発生しました")
			messageLogger.Error(map[string]string{
				"message": err.Error(),
			})
			return
		}
		defer db.Close()

		handlers.DiscordPostHandler(contexts.NewDiscordPostContext(
			context.Background(),
			s,
			m,
			db,
		))
	})

	discord.StateEnabled = true
	err = discord.Open()
	if err != nil {
		panic(err)
	}
	defer discord.Close()

	initializeLogger.Info(map[string]string{
		"message": "DiscordCommand session opened",
	})

	c := cron.New()
	cronLogger := logger.GetLogger("cron_handler", map[string]string{})
	for _, h := range handlers.GetCronHandlers() {
		handler := h
		c.AddFunc(handler.Spec, func() {
			db, err := db.NewSQLite3DB()
			if err != nil {
				cronLogger.Error(map[string]string{
					"message": err.Error(),
				})
				return
			}
			handler.Handler(contexts.NewCronContext(
				context.Background(),
				discord,
				db,
			))
		})
	}
	c.Start()

	initializeLogger.Info(map[string]string{
		"message": "Cron started",
	})

	println("Bot is running. Press CTRL-C to exit.")
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-stop
}
