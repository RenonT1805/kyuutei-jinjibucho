package handlers

import (
	"errors"
	"fmt"

	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/controllers"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/infrastructures"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/presenters"
)

type CronHandler struct {
	Spec    string
	Handler func(ctx *contexts.CronContext)
}

func GetCronHandlers() []CronHandler {
	return []CronHandler{
		// 毎週の進捗評価
		{
			Spec: "0 0 * * *",
			Handler: func(ctx *contexts.CronContext) {
				logger := logger.GetLogger("evaluation_cron.everyday", map[string]string{})
				evaluationController := controllers.NewEvaluationCronController(
					application.NewPostApplication(
						infrastructures.NewGuildSqlite3(ctx.GetDB()),
						infrastructures.NewPostSqlite3(ctx.GetDB()),
						infrastructures.NewPostSourceDiscord(ctx.GetDiscordSession(), ctx.GetDB()),
					),
					application.NewEvaluationApplication(
						presenters.NewEvaluationCronPresenter(ctx),
						infrastructures.NewGuildSqlite3(ctx.GetDB()),
						infrastructures.NewUserSqlite3(ctx.GetDB()),
						infrastructures.NewPostSqlite3(ctx.GetDB()),
						infrastructures.NewReactionSqlite3(ctx.GetDB()),
						infrastructures.NewReactionTypeSqlite3(ctx.GetDB()),
						infrastructures.NewStatSqlite3(ctx.GetDB()),
					),
					ctx,
				)

				err := evaluationController.Evaluation()
				if err != nil {
					logger.Error(map[string]string{
						"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
					})
					return
				}
			},
		},
		// 毎月の集計結果発表
		{
			Spec:    "0 0 1 * *",
			Handler: func(ctx *contexts.CronContext) {},
		},
	}
}
