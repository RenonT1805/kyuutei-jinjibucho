package handlers

import (
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/controllers"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/controllers/params"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/middlewares"
)

func ParseParam[T interface{}](ctx *contexts.DiscordCommandContext, logger logger.Logger) (*T, error) {
	params, err := params.ParseDiscordCommandParam[T](ctx)
	if err != nil {
		logger.Error(map[string]string{
			"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
		})
		ctx.GetDiscordSession().ChannelMessageSend(
			ctx.GetInteraction().GuildID,
			err.Error(),
		)
		return nil, err
	}
	return &params, nil
}

func GetDiscordCommandCommands() []*discordgo.ApplicationCommand {
	return []*discordgo.ApplicationCommand{
		{
			Name:        "register",
			Description: "チャンネルを宮廷人事部長に登録します",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "day_of_week",
					Description: "評価曜日を指定します",
					Choices: []*discordgo.ApplicationCommandOptionChoice{
						{
							Name:  "日曜日",
							Value: params.DiscordCommandGuildRegisterParamDayOfWeekSunday,
						},
						{
							Name:  "月曜日",
							Value: params.DiscordCommandGuildRegisterParamDayOfWeekMonday,
						},
						{
							Name:  "火曜日",
							Value: params.DiscordCommandGuildRegisterParamDayOfWeekTuesday,
						},
						{
							Name:  "水曜日",
							Value: params.DiscordCommandGuildRegisterParamDayOfWeekWednesday,
						},
						{
							Name:  "木曜日",
							Value: params.DiscordCommandGuildRegisterParamDayOfWeekThursday,
						},
						{
							Name:  "金曜日",
							Value: params.DiscordCommandGuildRegisterParamDayOfWeekFriday,
						},
						{
							Name:  "土曜日",
							Value: params.DiscordCommandGuildRegisterParamDayOfWeekSaturday,
						},
					},
					Required: true,
				},
			},
		},
		{
			Name:        "stat",
			Description: "集計結果を表示します",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "aggregation_period",
					Description: "集計期間を指定します",
					Choices: []*discordgo.ApplicationCommandOptionChoice{
						{
							Name:  "月次",
							Value: params.DiscordCommandStatParamMonth,
						},
						{
							Name:  "トータル",
							Value: params.DiscordCommandStatParamTotal,
						},
					},
					Required: true,
				},
			},
		},
		{
			Name:        "users",
			Description: "現在評価対象になっているユーザーを表示します。また、ギルドに所属するユーザーを追加できます",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionBoolean,
					Name:        "aggregation_period",
					Description: "集計期間を指定します",
					Required:    false,
				},
			},
		},
	}
}

func GetDiscordCommandCommandHandlers(
	middlewares []middlewares.DiscordCommandMiddlewere,
) map[string]func(
	ctx *contexts.DiscordCommandContext,
) {
	return map[string]func(ctx *contexts.DiscordCommandContext){
		"register": func(ctx *contexts.DiscordCommandContext) {
			logger := logger.GetLogger("discord_command_handler.register", map[string]string{})
			params, err := ParseParam[params.DiscordCommandGuildRegisterParam](ctx, logger)
			err = controllers.NewGuildDiscordCommandController(ctx).Register(params)
			if err != nil {
				logger.Error(map[string]string{"message": fmt.Sprintf("%+v", errors.Unwrap(err))})
			}
		},
		"stat": func(ctx *contexts.DiscordCommandContext) {
			logger := logger.GetLogger("discord_command_handler.stat", map[string]string{})
			params, err := ParseParam[params.DiscordCommandStatParam](ctx, logger)
			err = controllers.NewStatDiscordCommandController(ctx).Stat(params)
			if err != nil {
				logger.Error(map[string]string{"message": fmt.Sprintf("%+v", errors.Unwrap(err))})
			}
		},
		"users": func(ctx *contexts.DiscordCommandContext) {
			logger := logger.GetLogger("discord_command_handler.users", map[string]string{})
			params, err := ParseParam[params.DiscordCommandUsersParam](ctx, logger)
			err = controllers.NewGuildDiscordCommandController(ctx).Users(params)
			if err != nil {
				logger.Error(map[string]string{"message": fmt.Sprintf("%+v", errors.Unwrap(err))})
			}
		},
	}
}
