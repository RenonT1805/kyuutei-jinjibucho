package handlers

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/RenonT1805/kyuutei-jinjibucho/config"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/repositories"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/logger"
)

func DiscordPostHandler(ctx *contexts.DiscordPostContext) {
	logger := logger.GetLogger("discord_post_handler", map[string]string{})
	m := ctx.GetMessage()
	s := ctx.GetDiscordSession()

	db := ctx.GetDB()

	// Guildに登録されたチャンネルか
	var guildID string
	err := db.QueryRow("SELECT id FROM guilds WHERE id = ? AND channel_id = ?", m.GuildID, m.ChannelID).Scan(&guildID)
	if !repositories.IsNoRowsError(err) && err != nil {
		logger.Error(map[string]string{
			"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
		})
		return
	} else if repositories.IsNoRowsError(err) {
		return
	}

	if strings.HasPrefix(m.Message.Content, config.GetPostKeyword()) {
		err := s.MessageReactionAdd(m.ChannelID, m.ID, "👍")
		if err != nil {
			logger.Error(map[string]string{
				"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
			})
			return
		}
		err = s.MessageReactionAdd(m.ChannelID, m.ID, "👎")
		if err != nil {
			logger.Error(map[string]string{
				"message": fmt.Sprintf("%+v", errors.Unwrap(err)),
			})
			return
		}
	} else {
		s.ChannelMessageDelete(m.ChannelID, m.ID)
	}
}
