module gitlab.com/RenonT1805/kyuutei-jinjibucho

go 1.21.4

require (
	dario.cat/mergo v1.0.0
	github.com/bwmarrin/discordgo v0.27.1
	github.com/cockroachdb/errors v1.11.1
	github.com/fluent/fluent-logger-golang v1.9.0
	github.com/mattn/go-sqlite3 v1.14.18
	github.com/robfig/cron/v3 v3.0.1
	gopkg.in/go-playground/validator.v9 v9.31.0
)

require (
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/cockroachdb/logtags v0.0.0-20230118201751-21c54148d20b // indirect
	github.com/cockroachdb/redact v1.1.5 // indirect
	github.com/getsentry/sentry-go v0.18.0 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/philhofer/fwd v1.1.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/tinylib/msgp v1.1.9 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
