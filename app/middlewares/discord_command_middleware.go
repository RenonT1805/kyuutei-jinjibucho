package middlewares

import "gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"

type DiscordCommandMiddlewere interface {
	Handle(ctx *contexts.DiscordCommandContext) error
}
