package contexts

import (
	"context"
	"database/sql"

	"github.com/bwmarrin/discordgo"
)

type CronContext struct {
	context.Context
	session *discordgo.Session
	db      *sql.DB
}

func NewCronContext(
	ctx context.Context,
	session *discordgo.Session,
	db *sql.DB,
) *CronContext {
	return &CronContext{
		Context: ctx,
		session: session,
		db:      db,
	}
}

func (this *CronContext) GetDB() *sql.DB {
	return this.db
}

func (this *CronContext) GetDiscordSession() *discordgo.Session {
	return this.session
}
