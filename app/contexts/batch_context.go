package contexts

import (
	"context"
	"database/sql"

	"github.com/bwmarrin/discordgo"
)

type BatchContext struct {
	context.Context
	session *discordgo.Session
	db      *sql.DB
}

func NewBatchContext(
	ctx context.Context,
	session *discordgo.Session,
	db *sql.DB,
) *BatchContext {
	return &BatchContext{
		Context: ctx,
		session: session,
		db:      db,
	}
}

func (this *BatchContext) GetDB() *sql.DB {
	return this.db
}

func (this *BatchContext) GetDiscordSession() *discordgo.Session {
	return this.session
}
