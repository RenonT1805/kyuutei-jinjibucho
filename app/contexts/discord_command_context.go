package contexts

import (
	"context"
	"database/sql"

	"github.com/bwmarrin/discordgo"
)

type DiscordCommandContext struct {
	context.Context
	session     *discordgo.Session
	interaction *discordgo.InteractionCreate
	db          *sql.DB
}

func NewDiscordCommandContext(
	ctx context.Context,
	session *discordgo.Session,
	interaction *discordgo.InteractionCreate,
	db *sql.DB,
) *DiscordCommandContext {
	return &DiscordCommandContext{
		Context:     ctx,
		session:     session,
		interaction: interaction,
		db:          db,
	}
}

func (this *DiscordCommandContext) GetDB() *sql.DB {
	return this.db
}

func (this *DiscordCommandContext) GetInteraction() *discordgo.InteractionCreate {
	return this.interaction
}

func (this *DiscordCommandContext) GetDiscordSession() *discordgo.Session {
	return this.session
}
