package contexts

import (
	"context"
	"database/sql"

	"github.com/bwmarrin/discordgo"
)

type DiscordPostContext struct {
	context.Context
	session *discordgo.Session
	message *discordgo.MessageCreate
	db      *sql.DB
}

func NewDiscordPostContext(
	ctx context.Context,
	session *discordgo.Session,
	message *discordgo.MessageCreate,
	db *sql.DB,
) *DiscordPostContext {
	return &DiscordPostContext{
		Context: ctx,
		session: session,
		message: message,
		db:      db,
	}
}

func (this *DiscordPostContext) GetDB() *sql.DB {
	return this.db
}

func (this *DiscordPostContext) GetMessage() *discordgo.MessageCreate {
	return this.message
}

func (this *DiscordPostContext) GetDiscordSession() *discordgo.Session {
	return this.session
}
