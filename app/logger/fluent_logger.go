package logger

import (
	"log"
	"strconv"

	"dario.cat/mergo"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/config"
)

var logger *fluent.Fluent

type Logger interface {
	GetLogger(tag string, header map[string]string) Logger
	Info(data map[string]string)
	Error(data map[string]string)
	Fatal(data map[string]string)
	Warn(data map[string]string)
	Debug(data map[string]string)
	Post(data map[string]string)
}

type fluentLogger struct {
	tag    string
	logger *fluent.Fluent
	header map[string]string
}

func SetupLogger() error {
	portStr, err := config.GetLoggerPort()
	if err != nil {
		return errors.WithStack(err)
	}
	port, err := strconv.Atoi(portStr)
	if err != nil {
		return errors.WithStack(err)
	}
	host, err := config.GetLoggerHost()
	if err != nil {
		return errors.WithStack(err)
	}
	logger, err = fluent.New(fluent.Config{FluentPort: port, FluentHost: host})
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func CloseLogger() {
	logger.Close()
}

func GetLogger(tag string, header map[string]string) Logger {
	tag = config.LOGGING_TAG + "." + tag
	return &fluentLogger{
		tag:    tag,
		logger: logger,
		header: header,
	}
}

func (this *fluentLogger) GetLogger(tag string, header map[string]string) Logger {
	mergo.Merge(&this.header, header)
	return &fluentLogger{
		tag:    this.tag + "." + tag,
		logger: this.logger,
		header: this.header,
	}
}

func (this *fluentLogger) Info(data map[string]string) {
	data["level"] = "info"
	mergo.Merge(&data, this.header)
	err := this.logger.Post(this.tag, data)
	if err != nil {
		log.Fatalln(err)
	}
}

func (this *fluentLogger) Error(data map[string]string) {
	data["level"] = "error"
	mergo.Merge(&data, this.header)
	err := this.logger.Post(this.tag, data)
	if err != nil {
		log.Fatalln(err)
	}
}

func (this *fluentLogger) Fatal(data map[string]string) {
	data["level"] = "fatal"
	mergo.Merge(&data, this.header)
	err := this.logger.Post(this.tag, data)
	if err != nil {
		log.Fatalln(err)
	}
}

func (this *fluentLogger) Warn(data map[string]string) {
	data["level"] = "warn"
	mergo.Merge(&data, this.header)
	err := this.logger.Post(this.tag, data)
	if err != nil {
		log.Fatalln(err)
	}
}

func (this *fluentLogger) Debug(data map[string]string) {
	data["level"] = "debug"
	mergo.Merge(&data, this.header)
	err := this.logger.Post(this.tag, data)
	if err != nil {
		log.Fatalln(err)
	}
}

func (this *fluentLogger) Post(data map[string]string) {
	mergo.Merge(&data, this.header)
	err := this.logger.Post(this.tag, data)
	if err != nil {
		log.Fatalln(err)
	}
}
