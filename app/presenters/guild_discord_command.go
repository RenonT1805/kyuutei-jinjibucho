package presenters

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
)

type GuildDiscordCommandPresenter struct {
	ctx *contexts.DiscordCommandContext
}

func NewGuildDiscordCommandPresenter(ctx *contexts.DiscordCommandContext) *GuildDiscordCommandPresenter {
	return &GuildDiscordCommandPresenter{
		ctx: ctx,
	}
}

func (this *GuildDiscordCommandPresenter) RegisterComplete(output *application.GuildRegisterOutput) error {
	message := ""
	if output.AlreadyRegistered {
		message = "このサーバーにはすでにグループに登録されたチャンネルが存在している。サーバーには一つのチャンネルしか登録できない。"
	} else {
		message = "このサーバーの人事は只今から、宮廷人事部長が取り仕切る。このチャンネルで諸々の通知を行う。"
	}
	return this.ctx.GetDiscordSession().InteractionRespond(
		this.ctx.GetInteraction().Interaction,
		&discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: message,
			},
		},
	)
}
