package presenters

import (
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
)

type EvaluationCronPresenter struct {
	ctx *contexts.CronContext
}

func NewEvaluationCronPresenter(ctx *contexts.CronContext) *EvaluationCronPresenter {
	return &EvaluationCronPresenter{
		ctx: ctx,
	}
}

func (this *EvaluationCronPresenter) EvaluationComplete(output *application.EvaluationEvaluationOutput) error {
	for guildId, userScore := range output.ChannelUserScores {
		message := "今週の進捗の確認を行う\n"
		for userId, stats := range userScore {
			user, err := this.ctx.GetDiscordSession().User(userId)
			if err != nil {
				return errors.WithStack(err)
			}

			message += "\n" + user.Username + " の評価は以下の通りだ。\n"
			sum := 0
			for _, stat := range stats {
				message += "- " + stat.Description + " (" + strconv.Itoa(int(stat.Point)) + " ポイント)\n"
				sum += int(stat.Point)
			}

			if 0 < len(stats) {
				message += "合計 " + strconv.Itoa(sum) + " ポイント\n"
			} else {
				message += "お前はこの1週間何もしていない。無能はこのグループには必要ない。\n"
			}
		}

		message += "以上だ。引き続き進捗を出せ。"

		_, err := this.ctx.GetDiscordSession().ChannelMessageSend(guildId, message)
		if err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}
