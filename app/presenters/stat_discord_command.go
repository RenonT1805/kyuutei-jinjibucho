package presenters

import (
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/contexts"
	"gitlab.com/RenonT1805/kyuutei-jinjibucho/domain/application"
)

type StatDiscordCommandPresenter struct {
	ctx *contexts.DiscordCommandContext
}

func NewStatDiscordCommandPresenter(ctx *contexts.DiscordCommandContext) *StatDiscordCommandPresenter {
	return &StatDiscordCommandPresenter{
		ctx: ctx,
	}
}

func (this *StatDiscordCommandPresenter) StatComplete(output *application.StatStatOutput) error {
	sinceDt := time.Unix(output.Since, 0)
	sinceDtStr := sinceDt.Format("2006/01/02 15:04:05")
	message := sinceDtStr + " からの集計結果を発表する。\n"

	minuser := ""
	minpoint := 1000000
	for username, point := range output.UserSumPoints {
		message += username + ": " + strconv.Itoa(point) + " ポイント\n"
		if point < minpoint {
			minpoint = point
			minuser = username
		}
	}

	message += "最も無能だったのは " + minuser + " だ。\n"
	message += "無能はこのグループには必要ない。引き続き進捗を出せ。"

	return this.ctx.GetDiscordSession().InteractionRespond(
		this.ctx.GetInteraction().Interaction,
		&discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: message,
			},
		},
	)
}
