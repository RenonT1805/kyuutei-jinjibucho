package config

import (
	"errors"
	"os"
	"strings"
)

const DEFAULT_APP_PATH = "/workspace/app/build"
const CAMEL_APP_NAME = "KyuuteiJinjibucho"
const KEBAB_APP_NAME = "kyuutei-jinjibucho"
const SNAKE_APP_NAME = "KYUUTEI_JINJIBUCHO"
const SHORT_APP_NAME = "kyuutei-jinjibucho"
const LOGGING_TAG = "kyuutei.jinjibucho"

func getEnv(key string, defaultValue string, require bool) (string, error) {
	value := os.Getenv(key)
	if len(value) == 0 {
		if require {
			return "", errors.New("Environment variable " + key + " is required")
		}
		return defaultValue, nil
	}
	return value, nil
}

func GetAppDir() string {
	dir, _ := getEnv("HOME_DIR", DEFAULT_APP_PATH, false)
	if !strings.HasSuffix(dir, "/") && !strings.HasSuffix(dir, "\\") {
		dir += "/"
	}
	return dir
}

func GetBotToken() (string, error) {
	return getEnv("DISCORD_TOKEN", "", true)
}

func GetLoggerHost() (string, error) {
	return getEnv("FLUENTD_HOST", "localhost", false)
}

func GetLoggerPort() (string, error) {
	return getEnv("FLUENTD_PORT", "24224", false)
}

func GetEvaluationBatchNotification() (string, error) {
	return getEnv("EVALUATION_BATCH_NOTIFICATION", "FALSE", false)
}

func GetPostKeyword() string {
	return "進捗報告\n"
}
